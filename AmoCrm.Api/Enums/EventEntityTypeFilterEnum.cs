﻿namespace AmoCrm.Api.Enums;

public enum EventEntityTypeFilterEnum
{
    lead,
    contact,
    company,
    customer,
    task
}
