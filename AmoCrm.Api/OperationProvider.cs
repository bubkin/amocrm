﻿using AmoCrm.Api.Internal.Responses;

namespace AmoCrm.Api;

public class OperationProvider<T> where T : BaseArrayResponse?
{
    private readonly CancellationToken _ct;
    private readonly string _query;
    private readonly Func<string, CancellationToken, Task<T>> _callback;

    public OperationProvider(Func<string, CancellationToken, Task<T>> callback)
    {
        _callback = callback;
        _query = string.Empty;
        _ct = default;
    }

    public OperationProvider(Func<string, CancellationToken, Task<T>> callback, string query)
    {
        _callback = callback;
        _query = query;
        _ct = default;
    }

    public OperationProvider(Func<string, CancellationToken, Task<T>> callback, CancellationToken ct)
    {
        _callback = callback;
        _query = string.Empty;
        _ct = ct;
    }

    public OperationProvider(Func<string, CancellationToken, Task<T>> callback, string query, CancellationToken ct)
    {
        _callback = callback;
        _query = query;
        _ct = ct;
    }

    public OperationEnumerator<T> GetAsyncEnumerator()
    {
        return new OperationEnumerator<T>(_callback, _query, _ct);
    }
}