﻿using AmoCrm.Api.Common.Interfaces;

namespace AmoCrm.Api.Exceptions;

public class AmoCrmException : Exception, IAmoCrmException
{
    public AmoCrmException(string mess) : base(mess)
    { }
}
