﻿using AmoCrm.Api.Common.Interfaces;

namespace AmoCrm.Api.Exceptions;

public class GetAccessTokenException : Exception, IAmoCrmException
{
    public GetAccessTokenException(string mess) : base(mess)
    { }
}
