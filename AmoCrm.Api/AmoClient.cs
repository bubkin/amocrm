﻿using AmoCrm.Api.Exceptions;
using AmoCrm.Api.Internal.Requests;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace AmoCrm.Api;

public class AmoClient : IDisposable
{
    private readonly HttpClient _httpClient;
    private readonly AuthRequest _authRequest;
    private AuthResponse? _authCredentials;
    private bool disposedValue;

    public AmoClient(string clientId, string clientSecret, string redirectUri, string subdomain, string? authorizationCode = null, string? refreshToken = null)
    {
        _httpClient = new HttpClient(new SocketsHttpHandler
        {
            PooledConnectionLifetime = TimeSpan.FromMinutes(2),
        })
        {
            BaseAddress = new Uri($"https://{subdomain}.amocrm.ru")
        };

        _authRequest = new AuthRequest
        {
            ClientdId = clientId,
            ClientSecret = clientSecret,
            RedirectUri = redirectUri,
            AuthorizationCode = authorizationCode,
            RefreshToken = refreshToken
        };
    }

    private async Task GetAccessToken(int attempt = 1, CancellationToken cancellationToken = default)
    {
        using var message = new HttpRequestMessage(HttpMethod.Post, "oauth2/access_token");

        string dataStr = JsonConvert.SerializeObject(_authRequest, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        byte[] byteArray = Encoding.UTF8.GetBytes(dataStr);
        message.Content = new ByteArrayContent(byteArray);
        message.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

        using var resp = await _httpClient.SendAsync(message, cancellationToken);

        if (resp.IsSuccessStatusCode)
        {
            var strResult = await resp.Content.ReadAsStringAsync(cancellationToken: cancellationToken);
            var result = JsonConvert.DeserializeObject<AuthResponse>(strResult);
            if (result is null)
                throw new GetAccessTokenException($"InvalidCastException: {nameof(AuthResponse)}");

            _authCredentials = result;

            _authRequest.AuthorizationCode = null;
            _authRequest.RefreshToken = result.RefreshToken;
        }
        else
        {
            await Task.Delay(2_000, cancellationToken);
            attempt++;

            if (attempt < 4)
            {
                await GetAccessToken(attempt, cancellationToken);
                return;
            }
            else
            {
                var errorResp = await resp.Content.ReadAsStringAsync(cancellationToken);
                throw new GetAccessTokenException($"{resp.StatusCode}: {errorResp}");
            }
        }
    }

    internal async Task<JObject?> GetAsync(string url, int attempt = 1, CancellationToken cancellationToken = default)
    {
        if (_authCredentials is null)
            await GetAccessToken(cancellationToken: cancellationToken);

        using var message = new HttpRequestMessage(HttpMethod.Get, url);
        message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _authCredentials!.AccessToken);

        using var resp = await _httpClient.SendAsync(message, cancellationToken);

        if (resp.IsSuccessStatusCode)
        {
            var strResult = await resp.Content.ReadAsStringAsync(cancellationToken);
            if (string.IsNullOrEmpty(strResult))
                return null;

            return JObject.Parse(strResult);
        }

        attempt++;
        if (attempt > 5)
        {
            var errorResp = await resp.Content.ReadAsStringAsync(cancellationToken);
            throw new AmoCrmException($"{resp.StatusCode}: {errorResp}");
        }

        if (resp.StatusCode == HttpStatusCode.Unauthorized)
            await GetAccessToken(cancellationToken: cancellationToken);

        await Task.Delay(5_000, cancellationToken);

        return await GetAsync(url, attempt, cancellationToken);
    }

    internal async Task<JObject?> PostAsync<TValue>(string url, HttpMethod method, TValue data, int attempt = 1, CancellationToken cancellationToken = default)
    {
        if (_authCredentials is null)
            await GetAccessToken(cancellationToken: cancellationToken);

        using var message = new HttpRequestMessage(method, url);
        message.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _authCredentials!.AccessToken);

        string dataStr = JsonConvert.SerializeObject(data, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        byte[] byteArray = Encoding.UTF8.GetBytes(dataStr);
        message.Content = new ByteArrayContent(byteArray);
        message.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");

        using var resp = await _httpClient.SendAsync(message, cancellationToken);
        if (resp.IsSuccessStatusCode)
        {
            var strResult = await resp.Content.ReadAsStringAsync(cancellationToken);
            if (string.IsNullOrEmpty(strResult))
                return null;

            return JObject.Parse(strResult);
        }

        attempt++;
        if (attempt > 5)
        {
            var errorResp = await resp.Content.ReadAsStringAsync(cancellationToken);
            throw new AmoCrmException($"{resp.StatusCode}: {errorResp}");
        }

        if (resp.StatusCode == HttpStatusCode.Unauthorized)
            await GetAccessToken(cancellationToken: cancellationToken);

        await Task.Delay(5_000, cancellationToken);

        return await PostAsync(url, method, data, attempt, cancellationToken);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                _httpClient.Dispose();
            }

            _authRequest.RefreshToken = null;
            _authRequest.AuthorizationCode = null;
            _authCredentials = null;

            disposedValue = true;
        }
    }

    // TODO: переопределить метод завершения, только если "Dispose(bool disposing)" содержит код для освобождения неуправляемых ресурсов
    ~AmoClient()
    {
        // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
        Dispose(disposing: false);
    }

    public void Dispose()
    {
        // Не изменяйте этот код. Разместите код очистки в методе "Dispose(bool disposing)".
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}