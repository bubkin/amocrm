﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record UpdateLeadRequest : ModifiedLeadBaseRequest
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("_embedded")]
    public UpdateLeadEmbeddedRequest? Embedded { get; init; }
}
