﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record CreateLeadRequest : ModifiedLeadBaseRequest
{
    [JsonProperty("_embedded")]
    public CreateLeadEmbeddedRequest? Embedded { get; init; }
}
