﻿using AmoCrm.Api.Requests.Contact;
using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record CreateComplexLeadEmbeddedRequest
{
    [JsonProperty("contacts")]
    public ContactRequest[]? Contacts { get; init; }

    [JsonProperty("tags")]
    public CreateLeadEmbeddedEntityWithIdRequest[]? Tags { get; init; }
}
