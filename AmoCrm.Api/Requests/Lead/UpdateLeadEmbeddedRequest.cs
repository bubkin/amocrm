﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Lead;

public record UpdateLeadEmbeddedRequest
{
    [JsonProperty("tags")]
    public CreateLeadEmbeddedEntityWithIdRequest[]? Tags { get; init; }
}
