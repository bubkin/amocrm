﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Requests.Contact;

public record ContactRequest
{
    [JsonProperty("name")]
    public string? Name { get; init; }

    [JsonProperty("custom_fields_values")]
    public CustomFieldModel[]? CustomFields { get; init; }

    [JsonProperty("responsible_user_id")]
    public int? ResponsibleUserId { get; init; }

    [JsonProperty("created_by")]
    public int? CreatedBy { get; init; }

    [JsonProperty("created_at")]
    public long? CreatedAt { get; init; }
}
