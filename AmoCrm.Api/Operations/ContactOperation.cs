﻿using AmoCrm.Api.Extensions;
using AmoCrm.Api.Responses.Contact;

namespace AmoCrm.Api.Operations;

public class ContactOperation : OperationProvider<ContactArrayResponse?>
{
    internal ContactOperation(AmoClient amoClient, string query, CancellationToken ct) : base(amoClient.GetContactsAsync, query, ct)
    { }
}
