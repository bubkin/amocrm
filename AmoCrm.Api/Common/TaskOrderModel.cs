﻿using AmoCrm.Api.Enums;

namespace AmoCrm.Api.Common;

public record TaskOrderModel
{
    public TaskOrderEnum OrderField { get; init; }
    public OrderEnum Order { get; init; }
}
