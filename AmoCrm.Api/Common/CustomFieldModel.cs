﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Common;

public record CustomFieldModel
{
    [JsonProperty("field_id")]
    public int FieldId { get; init; }

    [JsonProperty("field_name")]
    public string FieldName { get; init; } = string.Empty;

    [JsonProperty("field_code")]
    public string FieldCode { get; init; } = string.Empty;

    [JsonProperty("field_type")]
    public string FieldType { get; init; } = string.Empty;

    [JsonProperty("values")]
    public CustomFieldValueModel[] Values { get; init; } = null!;
}
