﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Common;

public record CustomFieldValueModel
{
    [JsonProperty("value")]
    public string Value { get; init; } = string.Empty;
}
