﻿using AmoCrm.Api.Enums;

namespace AmoCrm.Api.Common;

public record EventEntityFilterModel
{
    public EventEntityTypeFilterEnum EntityType { get; init; }
    public int[] EntityIds { get; init; } = Array.Empty<int>();
}
