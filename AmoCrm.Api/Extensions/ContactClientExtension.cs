﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Operations;
using AmoCrm.Api.Requests.Contact;
using AmoCrm.Api.Responses.Contact;
using Newtonsoft.Json.Linq;
using System.Text;

namespace AmoCrm.Api.Extensions;

public static class ContactClientExtension
{
    private static string BuildQuery(int page,
        string? query,
        int? limit,
        string[] names,
        int[] createdByIds,
        int[] updatedByIds,
        int[] responsibleUserIds,
        PeriodModel? createdAtFilter,
        PeriodModel? updatedAtFilter,
        PeriodModel? closedAtFilter,
        CustomFieldRequest[] customFields,
        ContactOrderModel? order)
    {
        var queryBuilder = new StringBuilder(query);
        queryBuilder.Append($"&page={page}");

        if (limit.HasValue)
            queryBuilder.Append($"&limit={limit.Value}");

        if (names.Any())
            for (int i = 0; i < names.Length; i++)
                queryBuilder.Append($"&filter[name][{i}]={names[i]}");

        if (createdByIds.Any())
            for (int i = 0; i < createdByIds.Length; i++)
                queryBuilder.Append($"&filter[created_by][{i}]={createdByIds[i]}");

        if (updatedByIds.Any())
            for (int i = 0; i < updatedByIds.Length; i++)
                queryBuilder.Append($"&filter[updated_by][{i}]={updatedByIds[i]}");

        if (responsibleUserIds.Any())
            for (int i = 0; i < responsibleUserIds.Length; i++)
                queryBuilder.Append($"&filter[responsible_user_id][{i}]={responsibleUserIds[i]}");

        if (createdAtFilter is not null)
        {
            if (createdAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[created_at][from]={createdAtFilter.FromTicks.Value}");

            if (createdAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[created_at][to]={createdAtFilter.ToTicks.Value}");
        }

        if (updatedAtFilter is not null)
        {
            if (updatedAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][from]={updatedAtFilter.FromTicks.Value}");

            if (updatedAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][to]={updatedAtFilter.ToTicks.Value}");
        }

        if (closedAtFilter is not null)
        {
            if (closedAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[closest_task_at][from]={closedAtFilter.FromTicks.Value}");

            if (closedAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[closest_task_at][to]={closedAtFilter.ToTicks.Value}");
        }

        if (customFields.Any())
            for (int i = 0; i < customFields.Length; i++)
                queryBuilder.Append($"&filter[custom_fields_values][{customFields[i].FieldId}][{i}]={customFields[i].Value}");

        if (order is not null)
            queryBuilder.Append($"&order[{order.OrderField}]={order.Order}");

        var userQuery = queryBuilder.ToString();
        if (userQuery.StartsWith("&"))
            userQuery = userQuery[1..];

        return userQuery;
    }

    public static async Task<JObject?> GetContactsAsJObjectAsync(this AmoClient client, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder("/api/v4/contacts");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<ContactArrayResponse?> GetContactsAsync(this AmoClient client, string? query = null, CancellationToken cancellationToken = default)
    {
        var resp = await GetContactsAsJObjectAsync(client, query, cancellationToken);
        return resp?.ToObject<ContactArrayResponse>();
    }

    public static async Task<ContactArrayResponse?> GetContactsAsync(this AmoClient client, int page,
        string? query = null,
        int? limit = null,
        string[]? names = null,
        int[]? createdByIds = null,
        int[]? updatedByIds = null,
        int[]? responsibleUserIds = null,
        PeriodModel? createdAtFilter = null,
        PeriodModel? updatedAtFilter = null,
        PeriodModel? closedAtFilter = null,
        CustomFieldRequest[]? customFields = null,
        ContactOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        var contactQuery = BuildQuery(page,
            query,
            limit,
            names ?? Array.Empty<string>(),
            createdByIds ?? Array.Empty<int>(),
            updatedByIds ?? Array.Empty<int>(),
            responsibleUserIds ?? Array.Empty<int>(),
            createdAtFilter,
            updatedAtFilter,
            closedAtFilter,
            customFields ?? Array.Empty<CustomFieldRequest>(),
            order);

        return await GetContactsAsync(client, contactQuery, cancellationToken);
    }

    public static async Task<JObject?> GetContactAsJObjectAsync(this AmoClient client, int contactId, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder($"/api/v4/contacts/{contactId}");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<ContactResponse?> GetContactAsync(this AmoClient client, int contactId, CancellationToken cancellationToken = default)
    {
        var resp = await GetContactAsJObjectAsync(client, contactId, cancellationToken: cancellationToken);
        return resp?.ToObject<ContactResponse>();
    }

    public static async Task<JObject?> GeneralCreateContactAsync(this AmoClient client, object contact, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync("/api/v4/contacts", HttpMethod.Post, contact, cancellationToken: cancellationToken);
    }

    public static async Task<ContactArrayResponse?> CreateContactAsync(this AmoClient client, ContactRequest contact, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralCreateContactAsync(client, contact, cancellationToken);
        return resp?.ToObject<ContactArrayResponse>();
    }

    public static async Task<JObject?> GeneralUpdateContactAsync(this AmoClient client, int contactId, object contact, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync($"/api/v4/contacts/{contactId}", HttpMethod.Patch, contact, cancellationToken: cancellationToken);
    }

    public static async Task<ContactResponse?> UpdateContactAsync(this AmoClient client, int contactId, ContactRequest contact, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralUpdateContactAsync(client, contactId, contact, cancellationToken);
        return resp?.ToObject<ContactResponse>();
    }

    public static ContactOperation CreateContactOperation(this AmoClient client,
        string? query = null,
        int? limit = null,
        string[]? names = null,
        int[]? createdByIds = null,
        int[]? updatedByIds = null,
        int[]? responsibleUserIds = null,
        PeriodModel? createdAtFilter = null,
        PeriodModel? updatedAtFilter = null,
        PeriodModel? closedAtFilter = null,
        CustomFieldRequest[]? customFields = null,
        ContactOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        return new ContactOperation(client, BuildQuery(1,
            query,
            limit,
            names ?? Array.Empty<string>(),
            createdByIds ?? Array.Empty<int>(),
            updatedByIds ?? Array.Empty<int>(),
            responsibleUserIds ?? Array.Empty<int>(),
            createdAtFilter,
            updatedAtFilter,
            closedAtFilter,
            customFields ?? Array.Empty<CustomFieldRequest>(),
            order), cancellationToken);
    }
}
