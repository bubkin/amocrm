﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Operations;
using AmoCrm.Api.Requests.Task;
using AmoCrm.Api.Responses.Task;
using Newtonsoft.Json.Linq;
using System.Text;

namespace AmoCrm.Api.Extensions;

public static class TaskClientExtension
{
    private static string BuildQuery(
        string? query,
        int? page,
        int? limit,
        int[] responsibleUserIds,
        bool? isCompleted,
        int[] typeIds,
        TaskEntityFilterModel? entityFilterModel,
        int[] taskIds,
        PeriodModel? updatedAtFilter,
        TaskOrderModel? order
        )
    {
        var queryBuilder = new StringBuilder(query);

        if (page.HasValue)
            queryBuilder.Append($"&page={page.Value}");

        if (limit.HasValue)
            queryBuilder.Append($"&limit={limit.Value}");

        if (responsibleUserIds.Any())
            for (int i = 0; i < responsibleUserIds.Length; i++)
                queryBuilder.Append($"&filter[responsible_user_id][{i}]={responsibleUserIds[i]}");

        if (isCompleted.HasValue)
            queryBuilder.Append($"&filter[is_completed]={isCompleted.Value}");

        if (typeIds.Any())
            for (int i = 0; i < typeIds.Length; i++)
                queryBuilder.Append($"&filter[task_type][{i}]={typeIds[i]}");

        if (entityFilterModel is not null && entityFilterModel.EntityIds.Any())
        {
            queryBuilder.Append($"&filter[entity_type]={entityFilterModel.EntityType}");
            for (int i = 0; i < entityFilterModel.EntityIds.Length; i++)
                queryBuilder.Append($"&filter[entity_id][{i}]={entityFilterModel.EntityIds[i]}");
        }

        if (taskIds.Any())
            for (int i = 0; i < taskIds.Length; i++)
                queryBuilder.Append($"&filter[id][{i}]={taskIds[i]}");

        if (updatedAtFilter is not null)
        {
            if (updatedAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][from]={updatedAtFilter.FromTicks.Value}");

            if (updatedAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][to]={updatedAtFilter.ToTicks.Value}");
        }

        if (order is not null)
            queryBuilder.Append($"&order[{order.OrderField}]={order.Order}");

        var taskQuery = queryBuilder.ToString();
        if (taskQuery.StartsWith("&"))
            taskQuery = taskQuery[1..];

        return taskQuery;
    }

    internal static async Task<TaskArrayResponse?> GetTasksAsync(this AmoClient client, string query, CancellationToken ct)
    {
        var resp = await GetTasksAsJObjectAsync(client, query, ct);
        return resp?.ToObject<TaskArrayResponse>();
    }

    public static async Task<JObject?> GetTasksAsJObjectAsync(this AmoClient client, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder("/api/v4/tasks");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<TaskArrayResponse?> GetTasksAsync(this AmoClient client,
        string? query = null,
        int? page = null,
        int? limit = null,
        int[]? responsibleUserIds = null,
        bool? isCompleted = null,
        int[]? typeIds = null,
        TaskEntityFilterModel? entityFilterModel = null,
        int[]? taskIds = null,
        PeriodModel? updatedAtFilter = null,
        TaskOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        var taskQuery = BuildQuery(query, page, limit,
            responsibleUserIds ?? Array.Empty<int>(),
            isCompleted,
            typeIds ?? Array.Empty<int>(),
            entityFilterModel,
            taskIds ?? Array.Empty<int>(),
            updatedAtFilter,
            order);

        var resp = await GetTasksAsJObjectAsync(client, taskQuery, cancellationToken);
        return resp?.ToObject<TaskArrayResponse>();
    }

    public static async Task<JObject?> GeneralCreateTasksAsync(this AmoClient client, object tasks, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync("/api/v4/tasks", HttpMethod.Post, tasks, cancellationToken: cancellationToken);
    }

    public static async Task<ModifiedTaskResponse[]?> CreateTasksAsync(this AmoClient client, CreateTaskRequest[] tasks, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralCreateTasksAsync(client, tasks, cancellationToken);
        return resp?.SelectToken("_embedded.tasks")?.ToObject<ModifiedTaskResponse[]>();
    }

    public static async Task<JObject?> GeneralUpdateTasksAsync(this AmoClient client, object tasks, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync("/api/v4/tasks", HttpMethod.Patch, tasks, cancellationToken: cancellationToken);
    }

    public static async Task<ModifiedTaskResponse[]?> UpdateTasksAsync(this AmoClient client, UpdateTaskRequest[] tasks, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralUpdateTasksAsync(client, tasks, cancellationToken);
        return resp?.SelectToken("_embedded.tasks")?.ToObject<ModifiedTaskResponse[]>();
    }

    public static async Task<ModifiedTaskResponse[]?> CompleteTasksAsync(this AmoClient client, UpdateCompletedTaskRequest[] tasks, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralUpdateTasksAsync(client, tasks, cancellationToken);
        return resp?.SelectToken("_embedded.tasks")?.ToObject<ModifiedTaskResponse[]>();
    }

    public static TaskOperation CreateTaskOperation(this AmoClient client,
        string? query = null,
        int? limit = null,
        int[]? responsibleUserIds = null,
        bool? isCompleted = null,
        int[]? typeIds = null,
        TaskEntityFilterModel? entityFilterModel = null,
        int[]? taskIds = null,
        PeriodModel? updatedAtFilter = null,
        TaskOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        return new TaskOperation(client, BuildQuery(query, 1, limit,
            responsibleUserIds ?? Array.Empty<int>(),
            isCompleted,
            typeIds ?? Array.Empty<int>(),
            entityFilterModel,
            taskIds ?? Array.Empty<int>(),
            updatedAtFilter,
            order),
            cancellationToken);
    }
}
