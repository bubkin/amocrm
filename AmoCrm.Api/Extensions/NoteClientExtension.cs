﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Operations;
using AmoCrm.Api.Requests.Note;
using AmoCrm.Api.Responses.Note;
using Newtonsoft.Json.Linq;
using System.Text;

namespace AmoCrm.Api.Extensions;

public static class NoteClientExtension
{
    private static string BuildQuery(
        string? query,
        int? page,
        int? limit,
        int[] noteIds,
        int[] entityIds,
        string[] noteTypes,
        PeriodModel? updatedAtFilter,
        NoteOrderModel? order)
    {
        var queryBuilder = new StringBuilder(query);

        if (page.HasValue)
            queryBuilder.Append($"&page={page.Value}");

        if (limit.HasValue)
            queryBuilder.Append($"&limit={limit.Value}");

        if (noteIds.Any())
            for (int i = 0; i < noteIds.Length; i++)
                queryBuilder.Append($"&filter[id][{i}]={noteIds[i]}");

        if (entityIds.Any())
            for (int i = 0; i < entityIds.Length; i++)
                queryBuilder.Append($"&filter[entity_id][{i}]={entityIds[i]}");

        if (noteTypes.Any())
            for (int i = 0; i < noteTypes.Length; i++)
                queryBuilder.Append($"&filter[note_type][{i}]={noteTypes[i]}");

        if (updatedAtFilter is not null)
        {
            if (updatedAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][from]={updatedAtFilter.FromTicks.Value}");

            if (updatedAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][to]={updatedAtFilter.ToTicks.Value}");
        }

        if (order is not null)
            queryBuilder.Append($"&order[{order.OrderField}]={order.Order}");

        var noteQuery = queryBuilder.ToString();
        if (noteQuery.StartsWith("&"))
            noteQuery = noteQuery[1..];

        return noteQuery;
    }

    internal static async Task<NoteArrayResponse?> GetNotesAsync(this AmoClient client, string query, CancellationToken cancellationToken = default)
    {
        var resp = await client.GetAsync($"/api/v4/{query}", cancellationToken: cancellationToken);
        return resp?.ToObject<NoteArrayResponse>();
    }

    public static async Task<JObject?> GeneralCreateNotesAsync(this AmoClient client, string entityType, object notes, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync($"/api/v4/{entityType}/notes", HttpMethod.Post, notes, cancellationToken: cancellationToken);
    }

    public static async Task<CreateNoteResponse[]?> CreateNotesAsync(this AmoClient client, NoteRequest[] notes, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralCreateNotesAsync(client, "leads", notes, cancellationToken);
        return resp?.SelectToken("_embedded.notes")?.ToObject<CreateNoteResponse[]>();
    }

    public static async Task<JObject?> GetNotesAsJObjectAsync(this AmoClient client, string entityType, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder($"/api/v4/{entityType}/notes");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<NoteArrayResponse?> GetNotesAsync(this AmoClient client, string entityType,
        string? query = null,
        int? page = null,
        int? limit = null,
        int[]? noteIds = null,
        int[]? entityIds = null,
        string[]? noteTypes = null,
        PeriodModel? updatedAtFilter = null,
        NoteOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        var noteQuery = BuildQuery(query,
            page,
            limit,
            noteIds ?? Array.Empty<int>(),
            entityIds ?? Array.Empty<int>(),
            noteTypes ?? Array.Empty<string>(),
            updatedAtFilter,
            order);

        var resp = await GetNotesAsJObjectAsync(client, entityType, noteQuery, cancellationToken);
        return resp?.ToObject<NoteArrayResponse>();
    }

    public static NoteOperation CreateNoteOperation(this AmoClient client, string entityType,
        string? query = null,
        int? page = null,
        int? limit = null,
        int[]? noteIds = null,
        int[]? entityIds = null,
        string[]? noteTypes = null,
        PeriodModel? updatedAtFilter = null,
        NoteOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        var noteQuery = BuildQuery(query,
            page,
            limit,
            noteIds ?? Array.Empty<int>(),
            entityIds ?? Array.Empty<int>(),
            noteTypes ?? Array.Empty<string>(),
            updatedAtFilter,
            order);

        return new NoteOperation(client, $"{entityType}/notes?{noteQuery}", cancellationToken);
    }
}
