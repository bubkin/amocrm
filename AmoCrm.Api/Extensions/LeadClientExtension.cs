﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Operations;
using AmoCrm.Api.Requests.Lead;
using AmoCrm.Api.Responses.Lead;
using Newtonsoft.Json.Linq;
using System.Text;

namespace AmoCrm.Api.Extensions;

public static class LeadClientExtension
{
    private static string BuildQuery(
        string? query,
        int? page,
        int? limit,
        int[] leadIds,
        int[] pipelineIds,
        bool withContacts,
        int[] responsibleUserIds,
        CustomFieldRequest[] customFields,
        PeriodModel? createdAtFilter,
        PeriodModel? updatedAtFilter,
        PeriodModel? closedAtFilter,
        StatusFilterModel[] statusFilters,
        LeadOrderModel? order)
    {
        var queryBuilder = new StringBuilder(query);

        if (page.HasValue)
            queryBuilder.Append($"&page={page.Value}");

        if (limit.HasValue)
            queryBuilder.Append($"&limit={limit.Value}");

        if (leadIds.Any())
            for (int i = 0; i < leadIds.Length; i++)
                queryBuilder.Append($"&filter[id][{i}]={leadIds[i]}");

        if (pipelineIds.Any())
            for (int i = 0; i < pipelineIds.Length; i++)
                queryBuilder.Append($"&filter[pipeline_id][{i}]={pipelineIds[i]}");

        if (withContacts)
            queryBuilder.Append("&with=contacts");

        if (responsibleUserIds.Any())
            for (int i = 0; i < responsibleUserIds.Length; i++)
                queryBuilder.Append($"&filter[responsible_user_id][{i}]={responsibleUserIds[i]}");

        if (customFields.Any())
            for (int i = 0; i < customFields.Length; i++)
                queryBuilder.Append($"&filter[custom_fields_values][{customFields[i].FieldId}][{i}]={customFields[i].Value}");

        if (createdAtFilter is not null)
        {
            if (createdAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[created_at][from]={createdAtFilter.FromTicks.Value}");

            if (createdAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[created_at][to]={createdAtFilter.ToTicks.Value}");
        }

        if (updatedAtFilter is not null)
        {
            if (updatedAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][from]={updatedAtFilter.FromTicks.Value}");

            if (updatedAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[updated_at][to]={updatedAtFilter.ToTicks.Value}");
        }

        if (closedAtFilter is not null)
        {
            if (closedAtFilter.FromTicks.HasValue)
                queryBuilder.Append($"&filter[closed_at][from]={closedAtFilter.FromTicks.Value}");

            if (closedAtFilter.ToTicks.HasValue)
                queryBuilder.Append($"&filter[closed_at][to]={closedAtFilter.ToTicks.Value}");
        }

        if (statusFilters.Any())
            for (int i = 0; i < statusFilters.Length; i++)
                queryBuilder.Append($"&filter[statuses][{i}][pipeline_id]={statusFilters[i].PipelineId}&filter[statuses][{i}][status_id]={statusFilters[i].StatusId}");

        if (order is not null)
            queryBuilder.Append($"&order[{order.OrderField}]={order.Order}");

        var leadQuery = queryBuilder.ToString();
        if (leadQuery.StartsWith("&"))
            leadQuery = leadQuery[1..];

        return leadQuery;
    }

    internal static async Task<LeadArrayResponse?> GetLeadsAsync(this AmoClient client, string query, CancellationToken cancellationToken = default)
    {
        var resp = await GetLeadsAsJObjectAsync(client, query, cancellationToken);
        return resp?.ToObject<LeadArrayResponse>();
    }

    public static async Task<JObject?> GetLeadsAsJObjectAsync(this AmoClient client, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder("/api/v4/leads");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<LeadArrayResponse?> GetLeadsAsync(this AmoClient client,
        string? query = null,
        int? page = null,
        int? limit = null,
        int[]? leadIds = null,
        int[]? pipelineIds = null,
        bool withContacts = false,
        int[]? responsibleUserIds = null,
        CustomFieldRequest[]? customFields = null,
        PeriodModel? createdAtFilter = null,
        PeriodModel? updatedAtFilter = null,
        PeriodModel? closedAtFilter = null,
        StatusFilterModel[]? statusFilters = null,
        LeadOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        var leadQuery = BuildQuery(query, page, limit,
            leadIds ?? Array.Empty<int>(),
            pipelineIds ?? Array.Empty<int>(),
            withContacts,
            responsibleUserIds ?? Array.Empty<int>(),
            customFields ?? Array.Empty<CustomFieldRequest>(),
            createdAtFilter,
            updatedAtFilter,
            closedAtFilter,
            statusFilters ?? Array.Empty<StatusFilterModel>(),
            order);

        var resp = await GetLeadsAsJObjectAsync(client, leadQuery, cancellationToken);
        return resp?.ToObject<LeadArrayResponse>();
    }

    public static async Task<JObject?> GetLeadAsJObjectAsync(this AmoClient client, int leadId, string? filterQuery = null, CancellationToken cancellationToken = default)
    {
        var strBuilder = new StringBuilder($"/api/v4/leads/{leadId}");

        if (!string.IsNullOrEmpty(filterQuery) && !filterQuery.StartsWith("?"))
            strBuilder.Append('?');

        strBuilder.Append(filterQuery);

        return await client.GetAsync(strBuilder.ToString(), cancellationToken: cancellationToken);
    }

    public static async Task<LeadResponse?> GetLeadAsync(this AmoClient client, int leadId, bool withContacts = false, CancellationToken cancellationToken = default)
    {
        var leadQuery = BuildQuery(null, null, null, Array.Empty<int>(), Array.Empty<int>(), withContacts, Array.Empty<int>(), Array.Empty<CustomFieldRequest>(), null, null, null, Array.Empty<StatusFilterModel>(), null);

        var resp = await GetLeadAsJObjectAsync(client, leadId, leadQuery, cancellationToken);
        return resp?.ToObject<LeadResponse>();
    }

    public static async Task<JObject?> GeneralCreateLeadsAsync(this AmoClient client, object leads, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync("/api/v4/leads", HttpMethod.Post, leads, cancellationToken: cancellationToken);
    }

    public static async Task<CreateLeadResponse[]?> CreateLeadsAsync(this AmoClient client, CreateLeadRequest[] leads, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralCreateLeadsAsync(client, leads, cancellationToken);
        return resp?.SelectToken("_embedded.leads")?.ToObject<CreateLeadResponse[]>();
    }

    public static async Task<JObject?> GeneralCreateComplexLeadsAsync(this AmoClient client, object leads, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync("/api/v4/leads/complex", HttpMethod.Post, leads, cancellationToken: cancellationToken);
    }

    public static async Task<CreateComplexLeadResponse[]?> CreateComplexLeadsAsync(this AmoClient client, CreateComplexLeadRequest[] leads, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralCreateComplexLeadsAsync(client, leads, cancellationToken);
        return resp?.ToObject<CreateComplexLeadResponse[]>();
    }

    public static async Task<JObject?> GeneralUpdateLeadAsync(this AmoClient client, object leads, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync($"/api/v4/leads", HttpMethod.Patch, leads, cancellationToken: cancellationToken);
    }

    public static async Task<UpdateLeadResponse[]?> UpdateLeadAsync(this AmoClient client, UpdateLeadRequest[] leads, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralUpdateLeadAsync(client, leads, cancellationToken);
        return resp?.SelectToken("_embedded.leads")?.ToObject<UpdateLeadResponse[]>();
    }

    public static async Task<JObject?> GeneralUpdateLeadAsync(this AmoClient client, int leadId, object lead, CancellationToken cancellationToken = default)
    {
        return await client.PostAsync($"/api/v4/leads/{leadId}", HttpMethod.Patch, lead, cancellationToken: cancellationToken);
    }

    public static async Task<UpdateLeadResponse[]?> UpdateLeadAsync(this AmoClient client, int leadId, UpdateLeadRequest lead, CancellationToken cancellationToken = default)
    {
        var resp = await GeneralUpdateLeadAsync(client, leadId, lead, cancellationToken);
        return resp?.SelectToken("_embedded.leads")?.ToObject<UpdateLeadResponse[]>();
    }

    public static LeadOperation CreateLeadOperation(this AmoClient client, string? query = null,
        int? limit = null,
        int[]? leadIds = null,
        int[]? pipelineIds = null,
        bool withContacts = false,
        int[]? responsibleUserIds = null,
        CustomFieldRequest[]? customFields = null,
        PeriodModel? createdAtFilter = null,
        PeriodModel? updatedAtFilter = null,
        PeriodModel? closedAtFilter = null,
        StatusFilterModel[]? statusFilters = null,
        LeadOrderModel? order = null,
        CancellationToken cancellationToken = default)
    {
        return new LeadOperation(client, BuildQuery(query, 1, limit,
            leadIds ?? Array.Empty<int>(),
            pipelineIds ?? Array.Empty<int>(),
            withContacts,
            responsibleUserIds ?? Array.Empty<int>(),
            customFields ?? Array.Empty<CustomFieldRequest>(),
            createdAtFilter,
            updatedAtFilter,
            closedAtFilter,
            statusFilters ?? Array.Empty<StatusFilterModel>(),
            order),
            cancellationToken);
    }
}
