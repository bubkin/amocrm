﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Pipeline;

public record PipelineStatusResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("name")]
    public string Name { get; init; } = string.Empty;

    [JsonProperty("sort")]
    public int Sort { get; init; }

    [JsonProperty("is_editable")]
    public bool IsEditable { get; init; }

    [JsonProperty("pipeline_id")]
    public int PipelineId { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
