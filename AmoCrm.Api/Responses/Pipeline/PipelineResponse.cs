﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Internal.JsonConverters;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Pipeline;

[JsonConverter(typeof(JsonPathConverter))]
public record PipelineResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("name")]
    public string Name { get; init; } = string.Empty;

    [JsonProperty("sort")]
    public int Sort { get; init; }

    [JsonProperty("is_main")]
    public bool IsMain { get; init; }

    [JsonProperty("is_unsorted_on")]
    public bool IsUnsortedOn { get; init; }

    [JsonProperty("is_archive")]
    public bool IsArchive { get; init; }

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;

    [JsonProperty("_embedded.statuses")]
    public PipelineStatusResponse[] Statuses { get; init; } = null!;
}
