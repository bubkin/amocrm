﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Internal.JsonConverters;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Pipeline;

[JsonConverter(typeof(JsonPathConverter))]
public record PipelineArrayResponse
{
    [JsonProperty("_total_items")]
    public int TotalItems { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;

    [JsonProperty("_embedded.pipelines")]
    public PipelineResponse[] Pipelines { get; init; } = null!;
}
