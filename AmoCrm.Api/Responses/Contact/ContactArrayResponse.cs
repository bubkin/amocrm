﻿using AmoCrm.Api.Internal.JsonConverters;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Contact;

[JsonConverter(typeof(JsonPathConverter))]
public record ContactArrayResponse : BaseArrayResponse
{
    [JsonProperty("_embedded.contacts")]
    public ContactResponse[] Contacts { get; init; } = null!;
}
