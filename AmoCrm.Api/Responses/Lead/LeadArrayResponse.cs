﻿using AmoCrm.Api.Internal.JsonConverters;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

[JsonConverter(typeof(JsonPathConverter))]
public record LeadArrayResponse : BaseArrayResponse
{
    [JsonProperty("_embedded.leads")]
    public LeadResponse[] Leads { get; init; } = null!;
}
