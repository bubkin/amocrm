﻿using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record CreateComplexLeadResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("contact_id")]
    public int ContactId { get; init; }

    [JsonProperty("merged")]
    public bool Merged { get; init; }
}
