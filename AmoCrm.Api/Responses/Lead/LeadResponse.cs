﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Lead;

public record LeadResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("name")]
    public string Name { get; init; } = string.Empty;

    [JsonProperty("price")]
    public int Price { get; init; }

    [JsonProperty("responsible_user_id")]
    public int ResponsibleUserId { get; init; }

    [JsonProperty("group_id")]
    public int GroupId { get; init; }

    [JsonProperty("status_id")]
    public int StatusId { get; init; }

    [JsonProperty("pipeline_id")]
    public int PipelineId { get; init; }

    [JsonProperty("loss_reason_id")]
    public int? LossReasonId { get; init; }

    [JsonProperty("created_by")]
    public int CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int UpdatedBy { get; init; }

    [JsonProperty("created_at")]
    private long CreatedAtJson { get; init; }

    public DateTime CreatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(CreatedAtJson);

    [JsonProperty("updated_at")]
    public long UpdatedAtJson { get; init; }

    public DateTime UpdatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(UpdatedAtJson);

    [JsonProperty("closed_at")]
    public long? ClosedAtJson { get; init; }

    public DateTime? ClosedAtUtc => ClosedAtJson.HasValue ? new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(ClosedAtJson.Value) : null;

    [JsonProperty("closest_task_at")]
    public long? ClosestTaskAtJson { get; init; }

    public DateTime? ClosestTaskAtUtc => ClosestTaskAtJson.HasValue ? new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(ClosestTaskAtJson.Value) : null;

    [JsonProperty("is_deleted")]
    public bool IsDeleted { get; init; }

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("custom_fields_values")]
    public CustomFieldModel[]? CustomFields { get; init; }

    [JsonProperty("_embedded")]
    public LeadEmbeddedResponse? Embedded { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
