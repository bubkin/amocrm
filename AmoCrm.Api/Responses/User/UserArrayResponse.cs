﻿using AmoCrm.Api.Internal.JsonConverters;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.User;

[JsonConverter(typeof(JsonPathConverter))]
public record UserArrayResponse : BaseArrayResponse
{
    [JsonProperty("_total_items")]
    public int TotalItems { get; init; }

    [JsonProperty("_page_count")]
    public int PageCount { get; init; }

    [JsonProperty("_embedded.users")]
    public UserResponse[] Users { get; init; } = null!;
}
