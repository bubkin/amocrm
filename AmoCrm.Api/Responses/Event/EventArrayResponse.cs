﻿using AmoCrm.Api.Internal.JsonConverters;
using AmoCrm.Api.Internal.Responses;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Event;

[JsonConverter(typeof(JsonPathConverter))]
public record EventArrayResponse : BaseArrayResponse
{
    [JsonProperty("_embedded.events")]
    public EventResponse[] Events { get; init; } = null!;
}
