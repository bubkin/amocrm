﻿using AmoCrm.Api.Common;
using AmoCrm.Api.Internal.JsonConverters;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AmoCrm.Api.Responses.Event;

[JsonConverter(typeof(JsonPathConverter))]
public record EventResponse
{
    [JsonProperty("id")]
    public string Id { get; init; } = string.Empty;

    [JsonProperty("type")]
    public string Type { get; init; } = string.Empty;

    [JsonProperty("entity_id")]
    public int EntityId { get; init; }

    [JsonProperty("entity_type")]
    public string EntityType { get; init; } = string.Empty;

    [JsonProperty("created_by")]
    public int CreatedBy { get; init; }

    [JsonProperty("created_at")]
    public long CreatedAtJson { get; init; }
    public DateTime CreatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(CreatedAtJson);

    [JsonProperty("value_after")]
    public JToken[] ValueAfter { get; init; } = null!;

    [JsonProperty("value_before")]
    public JToken[] ValueBefore { get; init; } = null!;

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("_embedded.entity")]
    public JToken EmbeddedEntityToken { get; init; } = null!;

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;

    [JsonIgnore]
    public EventEmbeddedResponse? EmbeddedEntity =>
        EmbeddedEntityToken.HasValues ? EmbeddedEntityToken.Value<JObject>()?.ToObject<EventEmbeddedResponse>() : null;
}
