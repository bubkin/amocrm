﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Event;

public record EventEmbeddedResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
