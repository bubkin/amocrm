﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;

namespace AmoCrm.Api.Responses.Note;

public record NoteResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("entity_id")]
    public int EntityId { get; init; }

    [JsonProperty("created_by")]
    public int CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int UpdatedBy { get; init; }

    [JsonProperty("created_at")]
    public long CreatedAtJson { get; init; }
    public DateTime CreatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(CreatedAtJson);

    [JsonProperty("updated_at")]
    public long UpdatedAtJson { get; init; }
    public DateTime UpdatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(UpdatedAtJson);

    [JsonProperty("responsible_user_id")]
    public int ResponsibleUserId { get; init; }

    [JsonProperty("group_id")]
    public int GroupId { get; init; }

    [JsonProperty("note_type")]
    public string NoteType { get; init; } = string.Empty;

    [JsonProperty("params")]
    public Dictionary<string, string> Params { get; init; } = new();

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;
}
