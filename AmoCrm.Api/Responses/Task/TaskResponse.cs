﻿using AmoCrm.Api.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AmoCrm.Api.Responses.Task;

public record TaskResponse
{
    [JsonProperty("id")]
    public int Id { get; init; }

    [JsonProperty("created_by")]
    public int CreatedBy { get; init; }

    [JsonProperty("updated_by")]
    public int UpdatedBy { get; init; }

    [JsonProperty("created_at")]
    public long CreatedAtJson { get; init; }

    public DateTime CreatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(CreatedAtJson);

    [JsonProperty("updated_at")]
    public long UpdatedAtJson { get; init; }

    public DateTime UpdatedAtUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(UpdatedAtJson);

    [JsonProperty("responsible_user_id")]
    public int ResponsibleUserId { get; init; }

    [JsonProperty("group_id")]
    public int GroupId { get; init; }

    [JsonProperty("entity_id")]
    public int? EntityId { get; init; }

    [JsonProperty("entity_type")]
    public string? EntityType { get; init; }

    [JsonProperty("is_completed")]
    public bool IsCompleted { get; init; }

    [JsonProperty("task_type_id")]
    public int TaskTypeId { get; init; }

    [JsonProperty("text")]
    public string Text { get; init; } = string.Empty;

    [JsonProperty("duration")]
    public int Duration { get; init; }

    [JsonProperty("complete_till")]
    public long CompleteTillJson { get; init; }

    public DateTime CompleteTillUtc => new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(CompleteTillJson);

    [JsonProperty("account_id")]
    public int AccountId { get; init; }

    [JsonProperty("result")]
    private JToken ResultToken { get; init; } = null!;

    [JsonProperty("_links")]
    public LinksResponse Links { get; init; } = null!;

    [JsonIgnore]
    public TaskTextModel? Result =>
        ResultToken.HasValues ? ResultToken.Value<JObject>()?.ToObject<TaskTextModel>() : null;
}
